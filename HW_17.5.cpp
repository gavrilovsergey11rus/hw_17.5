#include <iostream>
#include <cmath>
#include <string>

class Vector
{
public:
    Vector() : x(5), y(5), z(5)
    {}
    Vector(double _x, double _y, double _z)
    {}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }

    void Module()
    {
        std::cout << "\nModul: " << sqrt( pow(x, 2) + pow(y, 2) + pow(z, 2) ) << std::endl;
    }

private:
    double x = 0;
    double y = 0;
    double z = 0;
};

class Animal
{
private:
    std::string Type;
    std::string Name;
    int Age;

public:

    Animal()
    {
        Type = "Dog";
        Name = "Simon";
        Age = 5;
    }

    Animal(std::string IType, std::string IName, int IAge)
    {
        Type = IType;
        Name = IName;
        Age = IAge;
    }



    void SetType(std::string String)
    {
        Type = String;
    }

    std::string GetType()
    {
        return Type;
    }

    void SetName(std::string String)
        {
            Name = String;
        }

    std::string GetName()
        {
            return Name;
        }

    void SetAge(int Int)
    {
        Age = Int;
    }

    int GetAge()
    {
        return Age;
    }
};



int main()
{
    Vector v;
    v.Show();
    std::cout << std::endl;
    v.Module();
    std::cout << std::endl;

    Animal a;
    a.SetType("Cat");
    a.SetName("Vasya");
    a.SetAge(3);
    std::cout << a.GetType() << ' ' << a.GetName() << ' ' << a.GetAge() << std::endl;

    Animal b("Pigg","Peppa",62);
    std::cout << b.GetType() << ' ' << b.GetName() << ' ' << b.GetAge() << std::endl;

    Animal c;
    std::cout << c.GetType() << ' ' << c.GetName() << ' ' << c.GetAge() << std::endl;
}